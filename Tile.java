public enum Tile{
	BLANK("_", "_"),
	WALL("W", "Wall"),
	HIDDEN_WALL("_", "Hidden Wall"),
	CASTLE("C", "Castle");
	
	private final String name;
	private final String longName;
	private Tile(String name, String longName){
		this.name = name;
		this.longName = longName;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getLongName(){
		return this.longName;
	}
	
}