import java.util.Random;
public class Board{
	
	private Tile[][] grid;
	private final int size;
	private final String BLUE = "\u001B[34m";
	private final String YELLOW = "\u001B[33m";
	private final String WHITE = "\u001B[37m";
	
	
	//Contructor
	public Board(){
		Random rng = new Random();
		this.size = 5;
		this.grid = new Tile[this.size][this.size];
		
		//Nested Loop to set tiles to _
		for(int i = 0; i<this.size;i++){
			for(int y = 0; y<this.size;y++){
				this.grid[i][y] = Tile.BLANK;
			}
			this.grid[i][rng.nextInt(this.size)] = Tile.HIDDEN_WALL;
		}
	}
	
	//toString
	public String toString(){
		String result = "0 " + BLUE + "1 2 3 4 5\n" + WHITE;
		for(int i = 0; i<this.size;i++){
			result += YELLOW + (i+1) + " " + WHITE;
			for(int y = 0; y<this.size;y++){
				result += this.grid[i][y].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	
	public int getSize(){
		return this.size;
	}


//!!!!QUARANtINED CODE!!!!
//FURTHER RESEARCH REQUIRED TO BE ABLE TO USE FOR EACH
/*
	public String toString(){
		String result = "";
		for(Tile[] row:this.grid){
			for(Tile tile:row){
				result += tile.getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
*/
	//Valid placement checker and placement method
	public boolean placeToken(int row, int col){
		if(this.grid[row-1][col-1] != Tile.BLANK){
			System.out.println("There is a " + this.grid[row-1][col-1].getLongName() + " stopping you from placing a castle there");
			if(this.grid[row-1][col-1] == Tile.HIDDEN_WALL){
				this.grid[row-1][col-1] = Tile.WALL;
			}
			return false;
		}else{
			this.grid[row-1][col-1] = Tile.CASTLE;
			return true;
		}
	}
	
	
	
}