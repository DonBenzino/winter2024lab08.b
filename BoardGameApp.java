import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		//Colours
		final String BLUE = "\u001B[34m";
		final String YELLOW = "\u001B[33m";
		final String WHITE = "\u001B[37m";
		
		//Variables
		System.out.println("Hello player");
		Board board = new Board();
		boolean gameOver = false;
		int Tries = 8;
		int Castles = 7;
		Scanner reader = new Scanner(System.in);
		
		//Main Loop
		while(!(gameOver)){
			int row = 0;
			int col = 0;
			System.out.println("Tries: " + Tries + " Castles: " + Castles);
			System.out.println(board);
			System.out.println(YELLOW + "In which row will you place your token" + WHITE);
			row = Integer.parseInt(reader.nextLine());
			System.out.println(BLUE + "In which column will you place your token" + WHITE);
			col = Integer.parseInt(reader.nextLine());
			
			//Secondary loop (if placement invalid)
			while(row < 1 || col < 1 || row > board.getSize() || col > board.getSize()){
				System.out.println("Invalid placement, try again");
				System.out.println("In which row will you place your token");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("In which column will you place your token");
				col = Integer.parseInt(reader.nextLine());
			}
			
			if(board.placeToken(row, col)){
				Castles--;
				System.out.println("You placed a Castle");
			}
			Tries--;
			reader.nextLine();
				
			//Win, tie or continue check
			if(Tries == 0){
				gameOver = true;
			}
			System.out.println("\033[H\033[2J");
		}
		//Win or loss declaration
		if(Castles == 0){
			System.out.println("You Win");
			System.out.println(board);
		}else{
			System.out.println("You lose");
			System.out.println(board);
		}
	}
}